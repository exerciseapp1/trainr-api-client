'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOCALSTORAGE_KEY = undefined;

var _es6Promise = require('es6-promise');

var _es6Promise2 = _interopRequireDefault(_es6Promise);

var _restClient = require('rest-client');

var _restClient2 = _interopRequireDefault(_restClient);

var _jqueryParam = require('jquery-param');

var _jqueryParam2 = _interopRequireDefault(_jqueryParam);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

_es6Promise2.default.polyfill();

var LOCALSTORAGE_KEY = 'trainr-api-client';

var TrainrAPIClient = function (_RESTClient) {
  _inherits(TrainrAPIClient, _RESTClient);

  function TrainrAPIClient(baseUrl) {
    _classCallCheck(this, TrainrAPIClient);

    var _this = _possibleConstructorReturn(this, (TrainrAPIClient.__proto__ || Object.getPrototypeOf(TrainrAPIClient)).call(this, baseUrl, {
      AUTH_PATH: '/auth/password/',
      LOGOUT_PATH: '/auth/logout/'
    }));

    _this.register = _this._resource({
      route: 'registration/password'
    });

    _this.forgotPassword = _this._resource({
      route: 'auth/forgot-password'
    });

    _this.resetPassword = _this._resource({
      route: 'auth/reset-password'
    });

    _this.users = _this._resource({
      route: 'users'
    });

    _this.notifications = _this._resource({
      route: 'notifications',
      details: [{ routeName: 'unread_count', method: 'get' }, { routeName: 'read', method: 'post' }, { routeName: 'read_all', method: 'post' }, { routeName: 'delete_all', method: 'post' }]
    });

    _this.profiles = _this._resource({
      route: 'profiles',
      details: [{ routeName: 'confirm', method: 'get' }]
    });

    _this.cities = _this._resource({
      route: 'cities',
      details: [{ routeName: 'with_bands', method: 'get' }, { routeName: 'with_venues', method: 'get' }]
    });

    _this.provinces = _this._resource({
      route: 'provinces',
      details: [{ routeName: 'with_bands', method: 'get' }, { routeName: 'with_venues', method: 'get' }]
    });

    _this.countries = _this._resource({
      route: 'countries',
      details: [{ routeName: 'with_bands', method: 'get' }, { routeName: 'with_venues', method: 'get' }]
    });

    _this.me = _this._resource({
      route: 'me'
    });

    _this.classes = _this._resource({
      route: 'classes'
    });

    _this.trainers = _this._resource({
      route: 'trainers'
    });

    _this.employers = _this._resource({
      route: 'employers'
    });

    return _this;
  }

  return TrainrAPIClient;
}(_restClient2.default);

exports.default = TrainrAPIClient;
exports.LOCALSTORAGE_KEY = LOCALSTORAGE_KEY;
