var path = require('path');

var config = {
  context: path.resolve(__dirname + '/src'),
  entry: ['./index.js',],
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname + '/dist'),
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: '/node_modules/',
        loader: 'babel-loader',
      }
    ]
  },
  devtool: '#inline-source-map'
};

module.exports = config;