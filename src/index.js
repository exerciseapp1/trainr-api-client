import es6Promise from 'es6-promise';
import RESTClient from 'rest-client';
es6Promise.polyfill()

import esc from 'jquery-param';

const LOCALSTORAGE_KEY = 'trainr-api-client';

class TrainrAPIClient extends RESTClient {
  constructor(baseUrl) {
    super(baseUrl, {
      AUTH_PATH: '/auth/password/',
      LOGOUT_PATH: '/auth/logout/'
    })

    this.register = this._resource({
      route: 'registration/password'
    })

    this.forgotPassword = this._resource({
      route: 'auth/forgot-password'
    })

    this.resetPassword = this._resource({
      route: 'auth/reset-password'
    })

    this.users = this._resource({
      route: 'users'
    })

    this.notifications = this._resource({
      route: 'notifications',
      details: [
        { routeName: 'unread_count', method: 'get' },
        { routeName: 'read', method: 'post' },
        { routeName: 'read_all', method: 'post' },
        { routeName: 'delete_all', method: 'post' }
      ]
    })

    this.profiles = this._resource({
      route: 'profiles',
      details: [
        { routeName: 'confirm', method: 'get' }
      ]
    })

    this.cities = this._resource({
      route: 'cities'
    })

    this.provinces = this._resource({
      route: 'provinces'
    })

    this.countries = this._resource({
      route: 'countries'
    })

    this.me = this._resource({
      route: 'me'
    })

    this.classes = this._resource({
      route: 'classes'
    })

    this.trainers = this._resource({
      route: 'trainers'
    })

    this.employers = this._resource({
      route: 'employers'
    })
  }
}

export default TrainrAPIClient;

export { LOCALSTORAGE_KEY }